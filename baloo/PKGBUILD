# Maintainer: Felix Yan <felixonmars@archlinux.org>
# Maintainer: Antonio Rojas <arojas@archlinux.org>
# Contributor: Andrea Scarpino <andrea@archlinux.org>

pkgname=baloo
pkgver=5.52.0
pkgrel=1.1
pkgdesc="A framework for searching and managing metadata"
arch=(x86_64)
url='https://community.kde.org/Frameworks'
license=(LGPL)
depends=(kfilemetadata kidletime kio lmdb)
makedepends=(extra-cmake-modules kdoctools doxygen qt5-tools qt5-declarative)
optdepends=('qt5-declarative: QML bindings')
groups=(kf5)
source=("https://download.kde.org/stable/frameworks/${pkgver%.*}/$pkgname-$pkgver.tar.xz"{,.sig}
D16999.patch::"https://cgit.kde.org/baloo.git/patch/?id=5b24bd42e4b646285a9161e02c4867082a54e3f5")
sha256sums=('8ddf85c6ae74dac868f37b562b80fdad753888f0c75b97ab26f01bdfc7bf5ea8'
            'SKIP'
            '92f7204a1640e3e8b9f5bfcdef264d3f98589574988d27ae08d823311b3a6b83')
validpgpkeys=(53E6B47B45CEA3E0D5B7457758D0EE648A48B3BB) # David Faure <faure@kde.org>

prepare() {
  mkdir -p build

  cd $pkgname-$pkgver
  msg "Add D16999 patch"
  patch -p1 -i ../D16999.patch
}

build() {
  cd build
  cmake ../$pkgname-$pkgver \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_LIBDIR=lib \
    -DBUILD_TESTING=OFF \
    -DBUILD_QCH=ON
  make -j5
}

package() {
  cd build
  make DESTDIR="$pkgdir" install
}
